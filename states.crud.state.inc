<?php

/**
 * Get the states a user can move to for a given node.
 *
 * @param object $node
 *   The node to check.
 * @return
 *   Array of transitions.
 */
function states_field_choices($node, $options = array()) {
  _states_default_options($options, $node);
  $from = $options['from'];
  
  $wid = states_get_flow_for_type($node->type);
  if (!$wid) {
    // No states for this type.
    return array();
  }
  $transitions = states_allowable_transitions($from, 'to', $options['roles']);
  // Include current state if it is not the (creation) state.
  if ($from == _states_creation_state($wid)) {
    unset($transitions[$from]);
  }
  return $transitions;
}


/**
 * Get the current state of a given node.
 *
 * @param $node
 *   The node to check.
 * @return
 *   The ID of the current state.
 */
function states_node_current_state($node) {
  $sid = FALSE;

  // There is no nid when creating a node.
  if (!empty($node->nid)) {
    $sid = db_query('SELECT sid FROM {states_node} WHERE nid = :nid', array(':nid' => $node->nid))->fetchField();
  }

  if (!$sid && !empty($node->type)) {
    // No current state. Use creation state.
    $wid = states_get_flow_for_type($node->type);
    $sid = _states_creation_state($wid);
  }
  return $sid;
}

/**
 * Get names and IDs of all states from the database.
 *
 * @return
 *   An array of states keyed by states ID.
 */
function states_get_all_flows() {
  $states = array();
  $result = db_query("SELECT wid, name FROM {states_flows} ORDER BY name ASC")->fetchAll();
  foreach ($result as $data) {
    $states[$data->wid] = check_plain(t($data->name));
  }
  return $states;
}


/**
 * Save a states's name in the database.
 *
 * @param $wid
 *   The ID of the statesl
 * @param $name
 *   The name of the states.
 * @param $tab_roles
 *   Array of role IDs allowed to see the states tab.
 * @param $options
 *   Array of key-value pairs that constitute various settings for
 *   this states. An example is whether to show the comment form
 *   on the states tab page or not.
 */
function states_update($wid, $name, $tab_roles, $options) {
  db_update('states')
    ->fields(array(
      'name' => $name,
      'tab_roles' => implode(',', $tab_roles),
      'options' => serialize($options),
    ))
    ->condition('wid', $wid)
    ->execute();
   // Workflow name change affects tabs (local tasks), so force menu rebuild.
  menu_rebuild();
}

/**
 * Load states for a flow from the database.
 * If $wid is not passed, all states for all flows are given.
 * States that have been deleted are not included.
 *
 * @param $wid
 *   The ID of the states.
 *
 * @return
 *   An array of states states keyed by state ID.
 */
function states_get_states($wid = NULL) {
  dsm($wid);
  $states = array();
  if (isset($wid)) {
    $states = db_query("SELECT sid, state FROM {states_states} WHERE wid = :wid AND status = 1 ORDER BY weight, sid", array(':wid' => $wid))->fetchAllAssoc('sid');
    foreach($states as $sid => $data) {
      $states[$sid] = check_plain(t($data->state));
    }
  }
  else {
    $states = db_query("SELECT ws.sid, ws.state, w.name FROM {states_states} ws INNER JOIN {states_flows} w ON ws.wid = w.wid WHERE status = 1 ORDER BY sid")->fetchAll();
    foreach($states as $sid => $data) {
      $states[$sid] = check_plain(t($data->name)) .': '. check_plain(t($data->state));
    }
  }

  return $states;
}

/**
 * Given the ID of a state, return a keyed array representing it.
 *
 * Note: this will retrieve states that have been deleted (their status key
 *   will be set to 0).
 *
 * @param $sid
 *   The ID of the states state.
 * @return
 *   A keyed array with all attributes of the state.
 */
function states_get_state($sid) {
  return db_query('SELECT wid, state, weight, sysid, status FROM {states_states} WHERE sid = :sid', array(':sid' => $sid))->fetchAssoc();
}

/**
 * Add or update a state to the database.
 *
 * @param $edit
 *   An array containing values for the new or updated states state.
 * @return
 *   The ID of the new or updated states state.
 */
function states_state_save($state) {
  if (!isset($state['sid'])) {
    drupal_write_record('states_states', $state);
    watchdog('states', 'Created states state %name', array('%name' => $form_state['values']['state']));
  }
  else {
    drupal_write_record('states_states', $state, 'sid');
  }

  return $state['sid'];
}


/**
 * Delete a state from the database, including any
 * transitions the state was involved in and any associations
 * with actions that were made to that transition.
 *
 * @param $sid
 *   The ID of the state to delete.
 * @param $new_sid
 *   Deleting a state will leave any nodes to which that state is assigned
 *   without a state. If $new_sid is given, it will be assigned to those
 *   orphaned nodes
 */
function states_state_delete($sid, $new_sid = NULL) {
  if ($new_sid) {
    // Assign nodes to new state so they are not orphaned.
    // A candidate for the batch API.
    $node = new stdClass();
    $node->states_stamp = time();
    $result = db_query("SELECT nid FROM {states_node} WHERE sid = :sid", array(':sid' => $sid))->fetchAll();
    foreach ($result as $data) {
      $node->nid = $data->nid;
      $node->_states = $sid;
      _states_write_history($node, $new_sid, t('Previous state deleted'));
      db_update('states_node')
        ->fields(array(
          'sid' => $sid,
        ))
        ->condition('nid', $data->nid)
        ->condition('sid', $sid)
        ->execute();
    }
  }
  else {
    // Go ahead and orphan nodes.
    db_delete('states_node')
      ->condition('sid', $sid)
      ->execute();
  }

  // Find out which transitions this state is involved in.
  $preexisting = array();
  $result = db_query("SELECT sid, target_sid FROM {states_transitions} WHERE sid = :sid OR target_sid = :target_sid", array('sid' => $sid, 'target_sid' => $sid))->fetchAll();
  foreach ($result as $data) {
   $preexisting[$data->sid][$data->target_sid] = TRUE;
  }

  // Delete the transitions and associated actions, if any.
  foreach ($preexisting as $from => $array) {
    foreach (array_keys($array) as $target_id) {
      $tid = states_get_transition_id($from, $target_id);
      states_transition_delete($tid);
    }
  }

  // Delete the state.
  db_update('states_states')
    ->fields(array(
      'status' => 0,
    ))
    ->condition('sid', $sid)
    ->execute();
  // Notify interested modules.
  $options = array('sid' => $sid);
  states_module_invoke('state delete', $options);
  $state_name = $states[$form_state['values']['sid']];
  watchdog('states', 'Deleted states state %name', array('%name' => $state_name));
}


