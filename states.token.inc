<?php

/**
 * Implementation of hook_token_values().
 */
function states_token_values($type, $object = NULL) {
  $values = array();
  switch ($type) {
    case 'node':
    case 'states':
      $node = (object)$object;

      if ($wid = states_get_states_for_type($node->type)) {
        $values['states-name'] = states_get_name($wid);
        $states = states_get_states($wid);
      }
      else {
        break;
      }

      $result = db_query_range("SELECT h.* FROM {states_node_history} h WHERE nid = %d ORDER BY stamp DESC", $node->nid, 0, 1);

      if ($row = db_fetch_object($result)) {
        $account = user_load(array('uid' => $row->uid));
        $comment = $row->comment;
      }

      $values['states-current-state-name']                =  $states[$row->sid];
      $values['states-old-state-name']                    =  $states[$row->old_sid];

      $values['states-current-state-date-iso']            =  date('Ymdhis',$row->stamp);
      $values['states-current-state-date-tstamp']         =  $row->stamp;
      $values['states-current-state-date-formatted']      =  date('M d, Y h:i:s', $row->stamp);

      $values['states-current-state-updating-user-name']  = $account->uid ? check_plain($account->name) : variable_get('anonymous', 'Anonymous');
      $values['states-current-state-updating-user-uid']   = $account->uid;
      $values['states-current-state-updating-user-mail']  = $account->uid ? check_plain($account->mail) : '';

      $values['states-current-state-log-entry']           = check_plain($row->comment);
      break;
  }

  return $values;
}

/**
 * Implementation of hook_token_list().
 */
function states_token_list($type = 'all') {

  if ($type == 'states' || $type == 'node' || $type == 'all') {
    $tokens['states']['states-name']                          =  'Name of states appied to this node';
    $tokens['states']['states-current-state-name']            =  'Current state of content';
    $tokens['states']['states-old-state-name']                =  'Old state of content';
    $tokens['states']['states-current-state-date-iso']        =  'Date of last state change (ISO)';
    $tokens['states']['states-current-state-date-tstamp']     =  'Date of last state change (timestamp)';
    $tokens['states']['states-current-state-date-formatted']  =  'Date of last state change (formated - M d, Y h:i:s)';;

    $tokens['states']['states-current-state-updating-user-name']       = 'Username of last state changer';
    $tokens['states']['states-current-state-updating-user-uid']        = 'uid of last state changer';
    $tokens['states']['states-current-state-updating-user-mail']       = 'email of last state changer';

    $tokens['states']['states-current-state-log-entry']       = 'Last states comment log';
    $tokens['node'] = $tokens['states'];
  }

  return $tokens;
}
