<?php

/**
 * Load function.
 *
 * @param $wid
 *   The ID of the states to load.
 * @return $states
 *   Object representing the states.
 */
function flow_load($wid) {
  $states = db_query('SELECT * FROM {states_flows} WHERE wid = :wid', array(':wid' => $wid))->fetchObject();
  if ($states) {
    $states->options = unserialize($states->options);
    return $states;
  }
  return FALSE;
}

/**
 * Given the ID of a flow, return its name.
 *
 * @param integer $wid
 *   The ID of the states.
 * @return string
 *   The name of the states.
 */
function states_get_flow_name($wid) {
  return db_query("SELECT name FROM {states_flows} WHERE wid = :wid", array(':wid' => $wid))->fetchField();
}

/**
 * Get ID of a flow for a node type.
 *
 * @param $type
 *   Machine readable node type name, e.g. 'story'.
 * @return int
 *   The ID of the states or FALSE if no states is mapped to this type.
 */
function states_get_flow_for_type($type) {
  static $cache;
  if(!isset($cache[$type])) {
    $wid = db_query("SELECT wid FROM {states_type_map} WHERE type = ':type'", array(':type' => $type))->fetchField();
    $cache[$type] = $wid;
  }
  else {
    $wid = $cache[$type];
  }
  return $wid > 0 ? $wid : FALSE;
}

/**
 * Create a flow and its (creation) state.
 *
 * @param $name
 *   The name of the states.
 */
function states_create_flow($name) {
  $flow = array(
    'name' => $name,
    'options' => serialize(array('comment_log_node' => 1, 'comment_log_tab' => 1)),
  );
  drupal_write_record('states_flows', $flow);
  states_state_save(array(
    'wid' => $flow['wid'],
    'state' => t('(creation)'),
    'sysid' => STATES_CREATION,
    'weight' => STATES_CREATION_DEFAULT_WEIGHT
  ));
  // Workflow creation affects tabs (local tasks), so force menu rebuild.
  menu_rebuild();
  watchdog('states', 'Created flow %name', array('%name' => $name));
  return $flow['wid'];
}

/**
 * Delete a flow from the database. Deletes all states,
 * transitions and node type mappings, too. Removes flow's state
 * information from nodes participating in this states.
 *
 * @param $wid
 *   The ID of the states.
 */
function states_delete_flow($wid) {
  $wf = states_get_flow_name($wid);
  $result = db_query('SELECT sid FROM {states_states} WHERE wid = :wid', array(':wid' => $wid))->fetchAll();
  foreach ($result as $data) {
    // Delete the state and any associated transitions and actions.
    states_state_delete($data->sid);
    db_delete('states_node')
      ->condition('sid', $data->sid)
      ->execute();
  }
  db_delete('states_type_map')
    ->condition('wid', $wid)
    ->execute();
  db_delete('states_flows')
    ->condition('wid', $wid)
    ->execute();

  // Notify any interested modules.
  $options = array('wid' => $wid);
  states_module_invoke('states delete', $options);
  
  // Workflow deletion affects tabs (local tasks), so force menu rebuild.
  cache_clear_all('*', 'cache_menu', TRUE);
  menu_rebuild();
  $states_name = states_get_flow_name($form_state['values']['wid']);
  watchdog('states', 'Deleted states %name with all its states', array('%name' => $states_name));
}

/**
 * Save mapping of flows to node type. E.g., "the story node type
 * is using the Foo flow."
 *
 * @param array $values
 */
function states_types_save_flow($values) {
  db_delete('states_type_map');
  $node_types = node_get_types();
  foreach ($node_types as $type => $name) {
    $map = array(
      'type' => $type,
      'wid' => $values[$type]['states'],
    );
    drupal_write_record('states_type_map', $map);
    variable_set('states_' . $type, array_keys(array_filter(($values[$type]['placement']))));
  }
}

/**
 * Return the ID of the creation state for this flow.
 *
 * @param $wid
 *   The ID of the states.
 */
function _states_creation_state($wid) {
  static $cache;
  if (!isset($cache[$wid])) {
    $cache[$wid] = db_query("SELECT sid FROM {states_states} WHERE wid = :wid AND sysid = :sysid", array(':wid' => $wid, ':sysid' => STATES_CREATION, ))->fetchField();
  }
  return $cache[$wid];
}