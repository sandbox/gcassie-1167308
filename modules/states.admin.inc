<?php
// $Id: state.admin.inc,v 1.7.2.1 2009/10/23 15:02:38 jvandyk Exp $

/**
 * @file
 * Administrative pages for configuring states.
 */

/**
 * Form builder. Create the form for adding/editing a state.
 *
 * @param $name
 *   Name of the state if editing.
 * @param $add
 *   Boolean, if true edit state name.
 *
 * @return
 *   HTML form.
 */
function states_manage_flow_form() {
  $form = array();
  
  $form['flow_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Workflow Name'),
    '#maxlength' => '254',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function states_add_flow_form($form, &$form_state) {
  $form = states_manage_flow_form();
  $form['submit']['#value'] = t('Add flow');
  $form['#validate'][] = 'states_manage_flow_form_validate';
  return $form;
}

function states_edit_flow_form($form, &$form_state, $flow) {
  $form = states_manage_flow_form();
  $form['flow_name']['#default_value'] = $flow->name;
  $form['flow'] = array(
    '#type' => 'value',
    '#value' => $flow,
  );
  $form['submit']['#value'] = t('Update flow');
  $form['#validate'][] = 'states_manage_flow_form_validate';

  return $form;
}

/**
 * Validate the state add/edit form.
 *
 * @see states_add_form()
 */
function states_manage_flow_form_validate($form, &$form_state) {
  $states_name = $form_state['values']['flow_name'];
  $states = array_flip(states_get_all_flows());
  // Make sure state name is not a duplicate.
  if (array_key_exists($states_name, $states)) {
    form_set_error('flow_name', t('A flow with the name %name already exists. Please enter another name for your new flow.',
      array('%name' => $states_name)));
  }
}

/**
 * Submit handler for the state add form.
 *
 * @see states_add_form()
 */
function states_add_flow_form_submit($form, &$form_state) {
  $states_name = $form_state['values']['flow_name'];
  $wid = states_create_flow($states_name);
  drupal_set_message(t('The flow %name was created. You should now add states to your flow.',
    array('%name' => $states_name)), 'status');
  $form_state['wid'] = $wid;
  $form_state['redirect'] = 'admin/structure/state/'. $wid;
}

function states_edit_flow_form_submit($form, &$form_state) {
  $flow = $form_state['values']['flow'];
  $flow->name = $form_state['values']['flow_name'];
  $flow->options = serialize($flow->options);
  drupal_write_record('states_flows', $flow, 'wid');
  $form_state['redirect'] = 'admin/structure/state/'. $flow->wid;
}

/**
 * Form structureer. Create form for confirmation of state deletion.
 *
 * @param $wid
 *   The ID of the state to delete.
 * @return
 *   Form definition array.
 *
 */
function states_delete_flow_form($form, &$form_state, $flow) {
  $form['flow'] = array(
    '#type' => 'value',
    '#value' => $flow,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to delete %title? All nodes that have a state associated with this flow will have those states removed.', array('%title' => $flow->name)),
    !empty($_GET['destination']) ? $_GET['destination'] : 'admin/structure/state',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for state deletion form.
 *
 * @see states_delete_form()
 */
function states_delete_flow_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1) {
    $flow = $form_state['values']['flow'];
    states_delete_flow($flow->wid);
    drupal_set_message(t('The flow %name with all its states was deleted.', array('%name' => $flow->name)));
    $form_state['redirect'] = 'admin/structure/state';
  }
}

/**
 * Menu callback and form builder. Create form to add a state state.
 *
 * @param $wid
 *   The ID of the state.
 * @return
 *   HTML form.
 */
function states_manage_state_form($flow) {
  $form['flow'] = array(
    '#type' => 'value',
    '#value' => $flow,
  );
  $form['state'] = array(
    '#type'  => 'textfield',
    '#title' => t('State name'),
    '#size'  => '16',
    '#maxlength' => '254',
    '#required' => TRUE,
    '#description' => t('Enter the name for a state in your state. For example, if you were doing a meal state it may include states like <em>shop</em>, <em>prepare</em>, <em>eat</em>, and <em>clean up</em>.'),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#description' => t('In listings, the heavier states will sink and the lighter states will be positioned nearer the top.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}

function states_add_state_form($form, &$form_state, $flow) {
  return states_manage_state_form($flow);
}

function states_edit_state_form($form, &$form_state, $flow, $sid) {
  $form = states_manage_state_form($flow);
  
  $state = states_get_state($sid);
  dsm($state);
  
  if ($state['status']) {
    drupal_set_title(t('Edit state state %state', array('%state' => $state['state'])), PASS_THROUGH);
    $form['state']['default_value'] = $state->name;
    $form['weight']['default_value'] = $state->weight;
  
    $form['state'] = array(
      '#type' => 'value',
      '#value' => $state,
    );
  }

  return $form;
}

/**
 * Validate the state addition form.
 *
 * @see states_states_add_form()
 */
function states_add_state_form_validate($form, &$form_state) {
  dsm($form_state);
  $states_name = $form_state['values']['state'];
  $wf_states = array_flip(states_get_states($form_state['values']['wid']));
  if (array_key_exists('sid', $form_state['values'])) {
    // Validate changes to existing state:
    // Make sure a nonblank state name is provided.
    if ($states_name == '') {
      form_set_error('state', t('Please provide a nonblank name for this state.'));
    }
    // Make sure changed state name is not a duplicate
    if (array_key_exists($states_name, $wf_states) && $form_state['values']['sid'] != $wf_states[$states_name]) {
      form_set_error('state', t('A state with the name %state already exists in this state. Please enter another name for this state.', array('%state' => $states_name)));
    }
  }
  else {
    // Validate new state:
    // Make sure a nonblank state name is provided
    if ($states_name == '') {
      form_set_error('state', t('Please provide a nonblank name for the new state.'));
    }
    // Make sure state name is not a duplicate
    if (array_key_exists($states_name, $wf_states)) {
      form_set_error('state', t('A state with the name %state already exists in this state. Please enter another name for your new state.', array('%state' => $states_name)));
    }
  }
}

/**
 * Submit handler for state addition form.
 *
 * @see states_states_add_form()
 */
function states_add_state_form_submit($form, &$form_state) {
  $form_state['sid'] = states_state_save($form_state['values']);
  if (array_key_exists('sid', $form_state['values'])) {
    drupal_set_message(t('The state state was updated.'));
  }
  else {
    drupal_set_message(t('The state state %name was created.', array('%name' => $form_state['values']['state'])));
  }
  $form_state['redirect'] = 'admin/structure/state';
}

/**
 * View state permissions by role
 *
 * @param $wid
 *   The ID of the state.
 */
function states_summary_page($flow) {
  $wid = $flow->wid;
  $name = $flow->name;
  $all = array();
  $roles = states_get_roles();
  foreach ($roles as $role => $value) {
    $all[$role]['name'] = $value;
  }
  $result = db_query(
    'SELECT t.roles, s1.state AS states_name, s2.state AS target_states_name 
    FROM {states_transitions} t 
    INNER JOIN {states_states} s1 ON s1.sid = t.sid 
    INNER JOIN {states_states} s2 ON s2.sid = t.target_sid 
    WHERE s1.wid = :wid 
    ORDER BY s1.weight ASC , s1.state ASC , s2.weight ASC , s2.state ASC',
    array(':wid' => $wid))->fetchAll();

  foreach ($result as $data) {
    foreach (explode(',', $data->roles) as $role) {
      $all[$role]['transitions'][] = array(check_plain(t($data->states_name)), WORKFLOW_ARROW, check_plain(t($data->target_states_name)));
    }
  }

  $header = array(t('From'), '', t('To'));
  return theme('states_permissions', array('header' => $header, 'all' => $all));
}

/**
 * Theme the state permissions view.
 */
function theme_states_permissions($var) {
  $output = '';
  foreach ($var['all'] as $role => $value) {
    $output .= '<h3>'. t("%role may do these transitions:", array('%role' => $value['name'])) .'</h3>';
    if (!empty($value['transitions'])) {
      $output .= theme('table', $var['header'], $value['transitions']) . '<p></p>';
    }
    else {
      $output .= '<table><tbody><tr class="odd"><td>' . t('None') . '</td><td></tr></tbody></table><p></p>';
    }
  }

  return $output;
}

/**
 * Menu callback. Edit a state's properties.
 *
 * @param $wid
 *   The ID of the state.
 * @return
 *   HTML form and permissions table.
 */
function states_edit_form($form_state, $state) {
  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $state->wid,
  );
  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workflow information')
  );
  $form['basic']['wf_name'] = array(
    '#type' => 'textfield',
    '#default_value' => $state->name,
    '#title' => t('Workflow Name'),
    '#size' => '16',
    '#maxlength' => '254',
  );
  $form['comment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comment for Workflow Log'),
  );
  $form['comment']['comment_log_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show a comment field in the state section of the editing form.'),
    '#default_value' => $state->options['comment_log_node'],
    '#description' => t("On the node editing form, a Comment form can be shown so that the person making the state change can record reasons for doing so. The comment is then included in the node's state history."),
  );
  $form['comment']['comment_log_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show a comment field in the state section of the state tab form.'),
    '#default_value' => $state->options['comment_log_tab'],
    '#description' => t("On the state tab, a Comment form can be shown so that the person making the state change can record reasons for doing so. The comment is then included in the node's state history."),
  );
  $form['tab'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workflow tab permissions'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tab']['tab_roles'] = array(
    '#type' => 'checkboxes',
    '#options' => states_get_roles(),
    '#default_value' => explode(',', $state->tab_roles),
    '#description' => t('Select any roles that should have access to the state tab on nodes that have a state.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

/**
 * Menu callback. Edit a state's transitions.
 *
 * @param $wid
 *   The ID of the state.
 * @return
 *   HTML form and permissions table.
 */
function states_trans_form($form_state, $state) {
  $form = array();
  $form['transitions']['#tree'] = TRUE;

  $roles = states_get_roles();
  $states = states_get_states($state->wid);

  if (!$states) {
    $form = array(
      '#type' => 'markup',
      '#value' => t('There are no states defined for this state.')
    );
    return $form;
  }
  foreach ($states as $states_id => $name) {
    foreach ($states as $nested_states_id => $nested_name) {
      if ($nested_name == t('(creation)')) {
        // Don't allow transition TO (creation).
        continue;
      }
      if ($nested_states_id != $states_id) {
        // Need to generate checkboxes for transition from $state to $nested_state.
        $from = $states_id;
        $to = $nested_states_id;
        foreach ($roles as $rid => $role_name) {
          $tid = states_get_transition_id($from, $to);
          $form['transitions'][$from][$to][$rid] = array(
            '#type' => 'checkbox',
            '#title' => check_plain($role_name),
            '#default_value' => $tid ? states_transition_allowed($tid, $rid) : FALSE);
        }
      }
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  
  $form['#state'] = $state;
  
  return $form;

}

/**
 * Theme the state editing form.
 *
 * @see states_edit_form()
 */
function theme_states_trans_form($form) {
  $state = $form['#state'];
  $output = drupal_render($form['wf_name']);
  $wid = $state->wid;
  $states = states_get_states($wid);
  drupal_set_title(t('Edit state %name', array('%name' => states_get_flow_name($wid))), PASS_THROUGH);
  if ($states) {
    $roles = states_get_roles();
    $header = array(array('data' => t('From / To ') . '&nbsp;' . WORKFLOW_ARROW));
    $rows = array();
    foreach ($states as $states_id => $name) {
      // Don't allow transition TO (creation).
      if ($name != t('(creation)')) {
        $header[] = array('data' => check_plain(t($name)));
      }
      $row = array(array('data' => check_plain(t($name))));
      foreach ($states as $nested_states_id => $nested_name) {
        if ($nested_name == t('(creation)')) {
          // Don't allow transition TO (creation).
          continue;
        }
        if ($nested_states_id != $states_id) {
          // Need to render checkboxes for transition from $state to $nested_state.
          $from = $states_id;
          $to = $nested_states_id;
          $cell = '';
          foreach ($roles as $rid => $role_name) {
            $cell .= drupal_render($form['transitions'][$from][$to][$rid]);
          }
          $row[] = array('data' => $cell);
        }
        else {
          $row[] = array('data' => '');
        }
      }
      $rows[] = $row;
    }
    $output .= theme('table', $header, $rows);

  }
  else {
    $output = t('There are no states defined for this state.');
  }

  $output .= drupal_render($form);
  return $output;
}

/**
 * Validate the state editing form.
 *
 * @see states_edit_form()
 */
function states_edit_form_validate($form_id, $form_state) {
  $wid = $form_state['values']['wid'];
  // Make sure state name is not a duplicate.
  if (array_key_exists('wf_name', $form_state['values']) && $form_state['values']['wf_name'] != '') {
    $states_name = $form_state['values']['wf_name'];
    $states = array_flip(states_get_all_flows());
    if (array_key_exists($states_name, $states) && $wid != $states[$states_name]) {
      form_set_error('wf_name', t('A state with the name %name already exists. Please enter another name for this state.',
        array('%name' => $states_name)));
    }
  }
  else {
  // No state name was provided.
    form_set_error('wf_name', t('Please provide a nonblank name for this state.'));
  }

  // Make sure 'author' is checked for (creation) -> [something].
  $creation_id = _states_creation_state($wid);
  if (isset($form_state['values']['transitions'][$creation_id]) && is_array($form_state['values']['transitions'][$creation_id])) {
    foreach ($form_state['values']['transitions'][$creation_id] as $to => $roles) {
      if ($roles['author']) {
        $author_has_permission = TRUE;
        break;
      }
    }
  }
}

/**
 * Submit handler for the state editing form.
 *
 * @see states_edit_form()
 */
function states_edit_form_submit($form, &$form_state) {
  states_update($form_state['values']['wid'], $form_state['values']['wf_name'], array_filter($form_state['values']['tab_roles']), array('comment_log_node' => $form_state['values']['comment_log_node'], 'comment_log_tab' => $form_state['values']['comment_log_tab']));
  drupal_set_message(t('The state was updated.'));
  $form_state['redirect'] = 'admin/structure/state';
}

function states_trans_form_validate($form_id, $form_state) {
  $wid = $form_state['values']['wid'];
  $states_count = db_result(db_query("SELECT COUNT(sid) FROM {states_states} WHERE wid = %d", $wid));
  if (empty($author_has_permission) && $states_count > 1) {
    form_set_error('transitions', t('Please give the author permission to go from %creation to at least one state!',
      array('%creation' => '(creation)')));
  }
}

/**
 * Submit handler for the state transition form.
 *
 * @see states_edit_form()
 */
function states_trans_form_submit($form, &$form_state) {
  states_update_transitions($form_state['values']['transitions']);
  drupal_set_message(t('The state was updated.'));
  //$form_state['redirect'] = 'admin/structure/state';
}

/**
 * Menu callback. Create the main state page, which gives an overview
 * of states and state states.
 */
function states_overview() {
  $states = states_get_all_flows();
  $row = array();

  foreach ($states as $wid => $name) {
    $links = array(
      'states_overview_add_state' => array(
        'title' => t('Add state'),
        'href' => "admin/structure/state/state/$wid"),
      'states_overview_edit' => array(
        'title' => t('Edit'),
        'href' => "admin/structure/state/edit/$wid"),
      'states_overview_delete' => array(
        'title' => t('Delete'),
        'href' => "admin/structure/state/delete/$wid"),
      'states_overview_trans' => array(
        'title' => t('Transitions'),
        'href' => "admin/structure/state/edit/$wid/trans"),
      'states_overview_summary' => array(
        'title' => t('Summary'),
        'href' => "admin/structure/state/edit/$wid/summary"),
    );

    // Allow modules to insert their own state operations.
    $more = module_invoke_all('states_operations', 'state', $wid);
    $links = array_merge($links, $more);

    $states = states_get_states($wid);
    if (!module_exists('trigger') || count($states) < 2) {
      unset($links['states_overview_actions']);
    }

    $row[] = array(check_plain(t($name)), theme('links', $links));
    $subrows = array();

    foreach ($states as $sid => $states_name) {
      $states_links = array();
      if (!states_is_system_state(check_plain(t($states_name)))) {
        $states_links = array(
          'states_overview_edit_state' => array(
            'title' => t('Edit'),
            'href' => "admin/structure/state/state/$wid/$sid",
          ),
          'states_overview_delete_state' => array(
            'title' => t('Delete'),
            'href' => "admin/structure/state/state/delete/$wid/$sid"
          ),
        );
      }
      // Allow modules to insert state operations.
      $states_links = array_merge($states_links, module_invoke_all('states_operations', 'state', $wid, $sid));
      $subrows[] = array(check_plain(t($states_name)), theme('links', $states_links));
      unset($states_links);
    }

    $subheader_state      = array('data' => t('State'), 'style' => 'width: 30%');
    $subheader_operations = array('data' => t('Operations'), 'style' => 'width: 70%');
    $subheader_style      = array('style' => 'width: 100%; margin: 3px 20px 20px;');
    $subtable = theme('table', array($subheader_state, $subheader_operations), $subrows, $subheader_style);

    $row[] = array(array('data' => $subtable, 'colspan' => '2'));
  }

  if ($row) {
    $output = theme('table', array(t('Workflow'), t('Operations')), $row);
    $output .= drupal_get_form('states_types_form');
  }
  else {
    $output = '<p>' . t('No states have been added. Would you like to <a href="@link">add a state</a>?', array('@link' => url('admin/structure/state/add'))) . '</p>';
  }

  return $output;
}

/**
 * Form builder. Create form for confirmation of deleting a state state.
 *
 * @param $wid
 *   integer The ID of the state.
 * @param $sid
 *   The ID of the state state.
 * @return
 *   HTML form.
 */
function states_states_delete_form($form_state, $wid, $sid) {
  $states = states_get_states($wid);
  $states_name = $states[$sid];

  // Will any nodes have no state if this state is deleted?
  if ($count = db_result(db_query("SELECT COUNT(nid) FROM {states_node} WHERE sid = %d", $sid))) {
    // Cannot assign a node to (creation) because that implies
    // that the node does not exist.
    $key = array_search(t('(creation)'), $states);
    unset($states[$key]);

    // Don't include the state to be deleted in our list of possible
    // states that can be assigned.
    unset($states[$sid]);
    $form['new_sid'] = array(
      '#type' => 'select',
      '#title' => t('State to be assigned to orphaned nodes'),
      '#description' => format_plural($count, 'Since you are deleting a state state, a node which is in that state will be orphaned, and must be reassigned to a new state. Please choose the new state.', 'Since you are deleting a state state, @count nodes which are in that state will be orphaned, and must be reassigned to a new state. Please choose the new state.'),
      '#options' => $states,
    );
  }

  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $wid
  );
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $sid
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete %title (and all its transitions)?', array('%title' => $states[$sid])),
    !empty($_GET['destination']) ? $_GET['destination'] : 'admin/structure/state',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for state state deletion form.
 *
 * @see states_states_delete_form()
 */
function states_states_delete_form_submit($form, &$form_state) {
  $states = states_get_states($form_state['values']['wid']);
  $states_name = $states[$form_state['values']['sid']];
  if ($form_state['values']['confirm'] == 1) {
    states_states_delete($form_state['values']['sid'], $form_state['values']['new_sid']);
    drupal_set_message(t('The state state %name was deleted.', array('%name' => $states_name)));
  }
  $form_state['redirect'] = 'admin/structure/state';
}

/**
 * Form builder. Allow administrator to map states to content types
 * and determine placement.
 */
function states_types_form() {
  $form = array();
  $states = array('<' . t('None') . '>') + states_get_all_flows();
  if (count($states) == 0) {
    return $form;
  }

  $type_map = db_query("SELECT wid, type FROM {states_type_map}")->fetchAllAssoc('type');

  $form['#theme'] = 'states_types_form';
  $form['#tree'] = TRUE;
  $form['help'] = array(
    '#type' => 'item',
    '#value' => t('Each content type may have a separate state. The form for changing state state can be displayed when editing a node, editing a comment for a node, or both.'),
  );

  foreach (node_type_get_names() as $type => $name) {
    $form[$type]['state'] = array(
      '#type' => 'select',
      '#title' => check_plain($name),
      '#options' => $states,
      '#default_value' => isset($type_map[$type]) ? $type_map[$type] : 0
    );
    $form[$type]['placement'] = array(
      '#type' => 'checkboxes',
      '#options' => array(
        'node' => t('Post'),
        'comment' => t('Comment'),
      ),
      '#default_value' => variable_get('states_' . $type, array('node')),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save state mapping')
  );
  return $form;
}

/**
 * Theme the state type mapping form.
 */
function theme_states_types_form($form) {
  $header = array(t('Content Type'), t('Workflow'), t('Display Workflow Form for:'));
  $rows = array();
  foreach (node_type_get_names() as $type => $name) {
    $name = $form[$type]['state']['#title'];
    unset($form[$type]['state']['#title']);
    $rows[] = array($name, drupal_render($form[$type]['state']), drupal_render($form[$type]['placement']));
  }
  $output = drupal_render($form['help']);
  $output .= theme('table', $header, $rows);
  return $output . drupal_render($form);
}

/**
 * Submit handler for state type mapping form.
 *
 * @see states_types_form()
 */
function states_types_form_submit($form, &$form_state) {
  states_types_save($form_state['values']);
  drupal_set_message(t('The state mapping was saved.'));
  menu_rebuild();
  $form_state['redirect'] = 'admin/structure/state';
}