<?php
// $Id: state.pages.inc,v 1.2.2.3 2010/02/16 03:44:19 jvandyk Exp $

/**
 * @file
 * Provide user interface for changing state state.
 */

/**
 * Menu callback. Display state summary of a node.
 */
function states_tab_page($node = NULL) {
  drupal_set_title(check_plain($node->title));
  $wid = states_get_states_for_type($node->type);
  $states_per_page = variable_get('states_states_per_page', 20);
  $states = db_query("SELECT sid, state FROM {states_states} WHERE status = 1 ORDER BY sid")->fetchAll();
  $deleted_states = db_query("SELECT sid, state FROM {states_states} WHERE status = 0 ORDER BY sid")->fetchAll();
  $current = states_node_current_state($node);

  // theme_states_current_state() must run state through check_plain().
  $output = '<p>'. t('Current state: !state', array('!state' => theme('states_current_state', $states[$current]))) . "</p>\n";
  $output .= drupal_get_form('states_tab_form', $node);
  
  $header = array();
  
  $history = db_select('states_node_history', 'h');
  $history->join('users', 'u', 'h.uid = u.uid');
  $history->condition('nid', $node->nid);
  $history->extend('PagerDefault')->limit($states_per_page);
  $history->extend('TableSort')->orderByHeader($header);
  $history->fetchAll();

  //$history = pager_query("SELECT h.*, u.name FROM {states_node_history} h LEFT JOIN {users} u ON h.uid = u.uid WHERE nid = %d ORDER BY hid DESC", $states_per_page, 0, NULL, $node->nid);
  $rows = array();
  while ($history = db_fetch_object($result)) {
    if ($history->sid == $current && !isset($deleted_states[$history->sid]) && !isset($current_themed)) {
      // Theme the current state differently so it stands out.
      $states_name = theme('states_current_state', $states[$history->sid]);
      // Make a note that we have themed the current state; other times in the history
      // of this node where the node was in this state do not need to be specially themed.
      $current_themed = TRUE;
    }
    elseif (isset($deleted_states[$history->sid])) {
      // The state has been deleted, but we include it in the history.
      $states_name = theme('states_deleted_state', $deleted_states[$history->sid]);
      $footer_needed = TRUE;
    }
    else {
      // Regular state.
      $states_name = check_plain(t($states[$history->sid]));
    }

    if (isset($deleted_states[$history->old_sid])) {
      $old_states_name = theme('states_deleted_state', $deleted_states[$history->old_sid]);
      $footer_needed = TRUE;
    }
    else {
      $old_states_name = check_plain(t($states[$history->old_sid]));
    }
    $rows[] = theme('states_history_table_row', $history, $old_states_name, $states_name);
  }
  $output .= theme('states_history_table', $rows, !empty($footer_needed));
  $output .= theme('pager', $states_per_page);
  return $output;
}

/**
 * Form builder. Allow state state change and scheduling from state tab.
 *
 * @param $node
 *   Node for which state information will be displayed.
 * @return
 *   Form definition array.
 */
function states_tab_form(&$form_state, $node) {
  $form['#wf_info']['tab'] = TRUE;

  states_node_form($form, $form_state, $node);

  $form['state']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
  );
  
  return $form;
}

/*
 * Theme one state history table row.
 *
 * $old_states_name and $states_name must be run through check_plain(t()) prior
 * to calling this theme function.
 */
function theme_states_history_table_row($history, $old_states_name, $states_name) {
  return array(
    format_date($history->stamp),
    $old_states_name,
    $states_name,
    theme('username', $history),
    filter_xss($history->comment, array('a', 'em', 'strong')),
  );
}

/*
 * Theme entire state history table.
 */
function theme_states_history_table($rows, $footer) {
  $output = theme('table', array(t('Date'), t('Old State'), t('New State'), t('By'), t('Comment')), $rows, array('class' => 'states_history'), t('Workflow History'));
  if ($footer) {
    $output .= t('*State is no longer available.');
  }
  return $output;
}

/**
 * Theme the current state in the state history table.
 */
function theme_states_current_state($states_name) {
  return '<strong>'. check_plain(t($states_name)) .'</strong>';
}

/**
 * Theme a deleted state in the state history table.
 */
function theme_states_deleted_state($states_name) {
  return check_plain(t($states_name)) .'*';
}

