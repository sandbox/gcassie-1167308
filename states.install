<?php
/**
 * Implementation of hook_install().
 */
function states_install() {
  drupal_install_schema('states');
}

/**
 * Implementation of hook_uninstall().
 */
function states_uninstall() {
  variable_del('states_states_per_page');
  // Delete type-states mapping variables.
  foreach (node_get_types() as $type => $name) {
    variable_del('states_'. $type);
  }
  drupal_uninstall_schema('states');
}

/**
 * Implementation of hook_schema().
 */
function states_schema() {
  $schema['states_flows'] = array(
    'fields'      => array(
      'wid'       => array(
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'name'      => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'tab_roles' => array(
        'type' => 'varchar',
        'length' => '60',
        'not null' => TRUE,
        'default' => '',
      ),
      'options'   => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('wid'),
  );
  $schema['states_type_map'] = array(
    'fields' => array(
      'type' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'wid'  => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'type' => array('type', 'wid'),
    ),
  );
  $schema['states_transitions'] = array(
    'fields' => array(
      'tid'        => array(
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'sid'        => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'target_sid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'roles'      => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('tid'),
    'indexes' => array(
      'sid' => array('sid'),
      'target_sid' => array('target_sid')),
  );
  $schema['states_states'] = array(
    'fields' => array(
      'sid'    => array(
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'wid'    => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'state'  => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'sysid'  => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'sysid' => array('sysid'),
      'wid' => array('wid')),
  );
  $schema['states_node_history'] = array(
    'fields' => array(
      'hid'     => array(
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'nid'     => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'old_sid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'sid'     => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid'     => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'stamp'   => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'comment' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('hid'),
    'indexes' => array(
      'nid' => array('nid', 'sid')),
  );
  $schema['states_node'] = array(
    'fields' => array(
      'nid'   => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'sid'   => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid'   => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'stamp' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('nid'),
    'indexes' => array(
      'nid' => array('nid', 'sid')),
  );

  return $schema;
}