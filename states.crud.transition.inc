<?php

/**
 * Validate target state and either let another module handle the transition
 * or execute it immediately. 
 *
 * @param $node
 * @param $to
 *   An integer; the target state ID.
 */
function states_transition($node, $to, $options = array()) {
  $options['to'] = $to;
  _states_default_options($options, $node);

  // Make sure new state is a valid choice.
  if (array_key_exists($to, states_field_choices($node, $options))) {
    // Let's see if any other modules take responsibility for this transition.
    // First module that returns a function name will win the honor.
    // TODO: this could be clearer
    $handler = module_invoke_all('states', 'handler', $options, $node);
    if ($handler) {
      module_invoke($handler[0], 'states_handler', $options, $node);
    }
    else {
      // Nobody else wants to deal with this. API handles the transition.
      // Could have defined this $op in states_states but then would
      //   have to worry even more about module weights.
      states_execute_transition($node, $to, $options);
    }
  }
}

/**
 * Execute a transition (change state of a node).
 *
 * @param $node
 * @param $to
 *   Target state ID.
 * @param $options
 *
 * @return int
 *   ID of new state.
 */
function states_execute_transition($node, $to, $options = array()) {
  $options['to'] = $to;
  _states_default_options($options, $node);
  $from = $options['from'];

  if (states_veto_check($node, $options)) {
    return;
  }

  // Stop if not going to a different state.
  // Write comment into history though.
  if ($from == $to && $options['states_comment']) {
    $node->states_stamp = time();
    db_update('states_node')->
      fields(array(
        'stamp', $node->states_stamp
      ))
      ->condition('nid', $node->nid)
      ->execute();
      
    states_module_invoke('transition pre', $options, $node);
    _states_write_history($node, $to, $options['states_comment']);
    states_module_invoke('transition post', $options, $node);
    
    return;
  }

  $tid = states_get_transition_id($from, $to);
  if (!$tid) {
    watchdog('states', 'Attempt to go to nonexistent transition (from %from to %to)', array('%from' => $from, '%to' => $to, WATCHDOG_ERROR));
    return;
  }
  
  // Make sure this transition is valid and allowed for the account.
  // Check allowability of state change if user is not superuser (might be cron).
  if ($options['roles'] != 'ALL' && !$options['force']) {
    if (!states_transition_allowed($tid, array_merge(array_keys($options['account']->roles), array('author')))) {
      watchdog('states', 'User %user not allowed to go from state %from to %to', array('%user' => $options['account']->name, '%from' => $form, '%to' => $to, WATCHDOG_NOTICE));
      return;
    }
  }
  
  // If the node does not have an existing $node->_states property, save
  // the $from there so _states_write_history() can log it.
  if (!isset($node->_states)) {
    $node->_states = $from;
  }
  states_module_invoke('transition pre', $options, $node);

  // Change the state.
  _states_node_to_state($node, $to, $options);
  $node->_states = $to;

  // Register state change with watchdog.
  $state = states_get_name($to);
  $type = node_get_types('name', $node->type);
  watchdog('states', 'State of @type %node_title set to %state_name', array('@type' => $type, '%node_title' => $node->title, '%state_name' => $state['name']), WATCHDOG_NOTICE, l('view', 'node/' . $node->nid));

  // Notify modules that transition has occurred. Actions should take place
  // in response to this callback, not the previous one.
  states_module_invoke('transition post', $options, $node);

  return $to;
}

/**
 * Update the transitions for flow.
 *
 * @param array $transitions
 *   Transitions, for example:
 *     18 => array(
 *       20 => array(
 *         'author' => 1,
 *         1        => 0,
 *         2        => 1,
 *       )
 *     )
 *   means the transition from state 18 to state 20 can be executed by
 *   the node author or a user in role 2. The $transitions array should
 *   contain ALL transitions for the states.
 */
function states_update_transitions($transitions = array()) {
  // Empty string is sometimes passed in instead of an array.
  if (!$transitions) {
    return;
  }

  foreach ($transitions as $from => $to_data) {
    foreach ($to_data as $to => $role_data) {
      foreach ($role_data as $role => $can_do) {
        if ($can_do) {
          states_transition_add_role($from, $to, $role);
        }
        else {
          states_transition_delete_role($from, $to, $role);
        }
      }
    }
  }
  db_delete('states_transitions')
    ->condition('roles', "''")
    ->execute();
}

/**
 * Add a role to the list of those allowed for a given transition.
 *
 * Add the transition if necessary.
 *
 * @param int $from
 * @param int $to
 * @param mixed $role
 *   Int (role ID) or string ('author').
 */
function states_transition_add_role($from, $to, $role) {
  $transition = array(
    'sid' => $from,
    'target_sid' => $to,
    'roles' => $role,
  );
  $tid = states_get_transition_id($from, $to);
  if ($tid) {
    $roles = _states_get_roles($tid);
    if (array_search($role, $roles) === FALSE) {
      $roles[] = $role;
      $transition['roles'] = implode(',', $roles);
      $transition['tid'] = $tid;
      drupal_write_record('states_transitions', $transition, 'tid');
    }
  }
  else {
    drupal_write_record('states_transitions', $transition);
  }
}

/**
 * Remove a role from the list of those allowed for a given transition.
 *
 * @param int $tid
 * @param mixed $role
 *   Int (role ID) or string ('author').
 */
function states_transition_delete_role($from, $to, $role) {
  $tid = states_get_transition_id($from, $to);
  if ($tid) {
    $roles = _states_get_roles($tid);
    if (($i = array_search($role, $roles)) !== FALSE) {
      unset($roles[$i]);
      db_update('states_transitions')
        ->fields(array(
          'roles' => implode(',', $roles)
        ))
        ->condition('tid', $tid)
        ->execute();
    }
  }
}


/**
 * See if a transition is allowed for a given role.
 *
 * @param int $tid
 * @param mixed $role
 *   A single role (int or string 'author') or array of roles.
 * @return
 *   TRUE if the role is allowed to do the transition.
 */
function states_transition_allowed($tid, $role = NULL) {
  if ($role) {
    if (!is_array($role)) {
      $role = array($role);
    }
    $allowed = _states_get_roles($tid);
    return array_intersect($role, $allowed) ==  TRUE;
  }
}

/**
 * Tell caller whether a state is a protected system state, such as the creation state.
 *
 * @param $state
 *   The name of the state to test
 * @return
 *   TRUE if the state is a system state.
 */
function states_is_system_state($state) {
  static $states;
  if (!isset($states)) {
    $states = array(t('(creation)') => TRUE);
  }
  return isset($states[$state]);
}

/**
 * Delete a transition (and any associated actions).
 *
 * @param $tid
 *   The ID of the transition.
 */
function states_transition_delete($tid) {
  db_delete('states_transitions')
    ->condition('tid', $tid)
    ->execute();
  // Notify interested modules.
  $options = array('tid' => $tid);
  states_module_invoke('transition delete', $options);
}

/**
 * Get allowable transitions for a given state. Typical use:
 *
 * global $user;
 * $possible = states_allowable_transitions($sid, 'to', $user->roles);
 *
 * If the state ID corresponded to the state named "Draft", $possible now
 * contains the states that the current user may move to from the Draft state.
 *
 * @param $sid
 *   The ID of the state in question.
 * @param $dir
 *   The direction of the transition: 'to' or 'from' the state denoted by $sid.
 *   When set to 'to' all the allowable states that may be moved to are
 *   returned; when set to 'from' all the allowable states that may move to the
 *   current state are returned.
 * @param mixed $roles
 *   Array of ints (and possibly the string 'author') representing the user's
 *   roles. If the string 'ALL' is passed (instead of an array) the role
 *   constraint is ignored (this is the default for backwards compatibility).
 *
 * @return
 *   Associative array of states ($sid => $state_name pairs), excluding current state.
 *
 * @TODO
 *   better DBNG
 */
function states_allowable_transitions($sid, $dir = 'to', $roles = 'ALL') {
  $transitions = array();

  if ($dir == 'to') {
    $field = 'target_sid';
    $field_where = 'sid';
  }
  else {
    $field = 'sid';
    $field_where = 'target_sid';
  }

  $result = db_query("(SELECT t.tid, t.:state_id as state_id, s.state AS state_name, s.weight AS state_weight 
    FROM {states_transitions} t 
    INNER JOIN {states_states} s 
    ON s.sid = t.:transition_id 
    WHERE t.:field_where = :state_id AND s.status = 1 
    ORDER BY state_weight) 
    UNION 
    (SELECT s.sid as tid, s.sid as state_id, s.state as state_name, s.weight as state_weight 
    FROM {states_states} s 
    WHERE s.sid = :state_id AND s.status = 1) 
    ORDER BY state_weight, state_id", array(':state_id' => $field, ':transition_id' => $field, ':field_where' => $field_where, ':state_id' => $sid, ))
    ->fetchAll();
    
  foreach($result as $data) {
    if ($roles == 'ALL'  // Superuser.
      || $sid == $data->state_id // Include current state for same-state transitions.
      || states_transition_allowed($data->tid, $roles)) {
      $transitions[$data->state_id] = check_plain(t($data->state_name));
    }
  }

  return $transitions;
}

/**
 * Get the tid of a transition, if it exists.
 *
 * @param int $from
 *   ID (sid) of originating state.
 * @param int $to
 *   ID (sid) of target state.
 * @return int
 *   Tid or FALSE if no such transition exists.
 */
function states_get_transition_id($from, $to) {
  return db_query("SELECT tid FROM {states_transitions} WHERE sid = :from AND target_sid = :to", array(':from' => $from, ':to' => $to))->fetchField();
}


/**
 * Put a node into a state.
 * No permission checking here; only call this from other functions that know
 * what they're doing.
 *
 * @see states_execute_transition()
 *
 * @param object $node
 * @param int $to
 */
function _states_node_to_state($node, $to, $options = array()) {
  _states_default_options($options, $node);
  $node->states_stamp = time();
  
  db_merge('states_node')
    ->key(array('nid' => $node->nid))
    ->fields(array(
      'sid' => $to,
      'uid' => $options['account']->uid,
      'stamp' => $node->states_stamp,
      ))
    ->execute();
  
  _states_write_history($node, $to, $options);
}


function _states_get_roles($tid) {
  return explode(',', db_query("SELECT roles FROM {states_transitions} WHERE tid = :tid", array(':tid', $tid))->fetchField());
}